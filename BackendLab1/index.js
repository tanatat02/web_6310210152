const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.post('/bmi', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if( !isNaN(weight) && !isNaN(height) ){
        let bmi = weight / (height * height)
        result = {
            "status" : 200,
            "bmi" : bmi
        }
    }else {

        result = {
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))

})

app.post('/area', (req, res) => {
  let get = parseFloat(req.query.get)
  let triangle = parseFloat(req.query.triangle)
  var result = {}

  if( !isNaN(get) && !isNaN(triangle) ){
    let area = (1 / 2) * (get * triangle)
    result = {
        "status" : 200,
        "area" : area
    }
    }else {

        result = {
            "status" : 400,
            "message" : "get or triangle is not a number"
        }
    }


    res.send(JSON.stringify(result))
})


app.post('/grade', (req, res) => {
  let post = (req.query.post)
  let score = parseFloat(req.query.score)
  var result = {}

    if(score <= 100 && score >= 80){
      grade = "A"
    } else if (score <= 79 && score >= 60) {
      grade = "B"
    } else if (score <= 59 && score >= 40) {
      grade = "C"
    } else {
      grade = "D"
    }

    result = {
      "post" : post,
      "score" : score,
      "grade" : grade
    }

    res.send(JSON.stringify(result))
})


app.get('/hello', (req, res) => {
    res.send('sawasdee ' + req.query.name)
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})