import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Address from './componentstwo/Address';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer /> <hr />
      <Address />
    </div>
  );
}

export default App;
