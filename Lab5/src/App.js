import logo from './logo.svg';
import './App.css';

import AboutUsPage from './page/AboutUsPage';
import Home from './page/Home';
import Header from './components/Header';

import LKNumber from './LuckyNumber/LKNumber';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element={
            <AboutUsPage />
          } />

          <Route path="/" element={
      <Home />
          } />

          <Route path="luckynumber" element={
      <LKNumber />
          } />

      
        
      </Routes>
    </div>
  );
}

export default App;
