
import BMIresult from "./BMIresult";
import{ useState } from "react";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";

function BMICalPage () {

    const  [ name, setName ] = useState("");
    const  [ bmiResult, setbmiResult ] = useState(0);
    const  [ translateResult, settranslateResult ] = useState("");

    
    const  [ height, setHeight ] = useState("");
    const  [ weight, setWeight ] = useState("");

function calculateBMI(){
    let h = parseFloat(height);
    let w = parseFloat(weight);
    let bmi = w / (h * h);
    setbmiResult( bmi );
    if (bmi > 25) {
        settranslateResult("อ้วน");
    }else{
        settranslateResult("ผอม")
    }

}

    return(
        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{marginTop : "10px"}}>
                <Grid item xs={12}>
                    <Typography variant="h5">
                        ยินดีต้อนรับสู่เว็บคำนวณ BMI
                    </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign : 'left'}}>
                        คุณชื่อ: <input type="text"
                                    value={name}
                                    onChange={ (e) => {setName(e.target.value); } } /> <br />
                                    <br />
                                    <br />
                        สูง: <input type="text"
                                    value={height}
                                    onChange={ (e) => {setHeight(e.target.value); } } /> <br />
                                    <br />
                                    <br />
                        หนัก: <input type="text"
                                    value={weight}
                                    onChange={ (e) => {setWeight(e.target.value); } } /> <br />
                                    <br />
                                    <br />

                        <Button variant="contained" onClick={ ()=>{ calculateBMI() } }>calculateBMI</Button>
                </Box>
                </Grid>
                <Grid item xs={4}>
                {  bmiResult != 0 &&
                        <div>
                            <hr />
                            มีผลคำนวณว่า            
                            <BMIresult
                                name= { name }
                                bmi= { bmiResult }
                                result= { translateResult }
                            />
                        </div>
                    }
                </Grid>
            </Grid>
        </Container>

    );
}
export default BMICalPage;

/*    return(
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr />

                คุณชื่อ: <input type="text"
                            value={name}
                            onChange={ (e) => {setName(e.target.value); } } /> <br />
                สูง: <input type="text"
                            value={height}
                            onChange={ (e) => {setHeight(e.target.value); } } /> <br />
                หนัก: <input type="text"
                            value={weight}
                            onChange={ (e) => {setWeight(e.target.value); } } /> <br />


                <Button variant="contained" onClick={ ()=>{ calculateBMI() } }>calculateBMI</Button>
 
                {  bmiResult != 0 &&
                    <div>
                        <hr />
                        มีผลคำนวณว่า

                                
                        <BMIresult
                            name= { name }
                            bmi= { bmiResult }
                            result= { translateResult }
                />
                </div>
                }
            </div>
        </div>
    );
}

export default BMICalPage; */