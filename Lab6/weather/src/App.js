import logo from './logo.svg';
import './App.css';

import { Box, AppBar, Toolbar, Typography, Card } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import { useEffect, useState } from 'react';
import axios from 'axios'
import ReactWeather, { useOpenWeather } from 'react-open-weather';

import YouTube from 'react-youtube';


function App() {

  const [ temperature, setTemperature ] = useState();
  

  const weatherAPIBaseUrl =
        "http://api.openweathermap.org/data/2.5/weather?";
  const city = "Songkhla";
        const apiKey = "1e90af0a190b86ee37b5f4d7781f7b7a"
        

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '1e90af0a190b86ee37b5f4d7781f7b7a',
    lat: '48.137154',
    lon: '11.576124',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });


            // OpenWeatherMap API Key   1e90af0a190b86ee37b5f4d7781f7b7a
        useEffect( () => {

        setTemperature("----")
    
        axios.get(weatherAPIBaseUrl+"q="+city+"&appid="+apiKey).then ( (response)=> {
          let data = response.data;
          let temp = data.main.temp - 273;
          setTemperature(temp.toFixed(2));
        })
  }
  , [] );
  return (
    <Box sx={{ flexGrow : 1, width : "100%"}}>
        <AppBar position="static">
          <Toolbar>
             <Typography variant="h6">
               Weater App
             </Typography>
          </Toolbar>
        </AppBar>

        <Box sx={{ justifyContent : 'center', marginTop : "20px",width : "100%", display : "flex" }}>
            <Typography variant="h4">
              อากาศหาดใหญ่ วันนี้
            </Typography>
          </Box>
        
        <Box sx={{ justifyContent : 'center', marginTop : "20px",width : "100%", display : "flex" }}>

        <Card sx={{ width: 275 }}>
                  <CardContent>
                      <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                        อุณหภูมิหาดใหญ่วันนี้คือ
                      </Typography>
                      <Typography variant="h5" component="div">
                        { temperature }
                      </Typography>
                      <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        องศาเซลเซียส
                      </Typography>
        
                  </CardContent>

          </Card>
          <ReactWeather
          isLoading={isLoading}
          errorMessage={errorMessage}
          data={data}
          lang="en"
          locationLabel="Munich"
          unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
          showForecast/>
      </Box>
      <YouTube videoId="5QhhwBumbTo" />
    </Box>
  );
}

export default App;
