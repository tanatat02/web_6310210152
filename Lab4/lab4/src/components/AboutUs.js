
function AboutUs (props) {



    return( 
    <div>
        <h2> จัดทำโดย: { props.name } </h2>
        <h3> ติดต่อได้ที่ { props.address } </h3>
        <h3> อยู่ที่ { props.province } </h3>
    </div>
    );

}

export default AboutUs;