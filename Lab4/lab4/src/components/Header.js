import { Link } from "react-router-dom";
function Header() {


    return(

        <div align="left">
            ยินดีต้อนรับสู่เว็บคำนวณ BMI :  
            <Link to="/">เครื่องคิดเลข</Link>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to="about">ผู้จัดทำ</Link>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to="luckynumber">ทายเลข</Link>
            <hr />
        </div>

    );

}

export default Header;