
import LKresult from "./LKresult";
import{ useState } from "react";

function LKNumber () {

    const  [ number, setNumber ] = useState("");
    const  [ numResult, setnumResult ] = useState(0);
    const  [ translateResult, settranslateResult ] = useState("");

function calculateLK(){
    let n = parseFloat(number);
    let num = n;
    setnumResult( num );
    if (num == 69) {
        settranslateResult("ถูกแล้วจ้า");
    }else{
        settranslateResult("ผิด")
    }

}

    return(
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บทายเลข
                <hr />
                กรุณาทายเลขที่ต้องการระหว่าง0-99: <input type="text"
                            value={number}
                            onChange={ (e) => {setNumber(e.target.value); } } /> <br />

                <button onClick={ ()=>{ calculateLK() } }> ทาย </button>
                {  numResult != 0 &&
                    <div>
                        <hr />
                                
                        <LKresult
                            num= { numResult }
                            result= { translateResult }
                        
                />
                </div>
                }
            </div>
        </div>
    );
}

export default LKNumber;