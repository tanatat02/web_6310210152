
import AboutUs from '../components/AboutUs';
function AboutUsPage(){

    return(
        <div>
            <div align="center">
                <h2> คณะผู้จัดทำ เว็บนี้ </h2>

                <AboutUs name="ธนทัต"
                         address="บ้าน"
                         province="สงขลา" />
                <hr />

            </div>
        </div>
    );
}

export default AboutUsPage;